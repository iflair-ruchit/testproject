<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('page_title', 'Admin Register')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="icon" href="{{asset('dist/img/favicon.ico')}}" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/register.css')}}">
  <script src="{{asset('js/register.js')}}"></script>
  <style type="text/css">
    .login-page{background-image: url('public/dist/img/bg-1.jpg')}
  </style>
</head>
<body class="login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{url('/')}}"><b>Admin</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <h6 class="login-box-msg">Register to New Account</h6>

      <form method="post" id="register_form" action="{{route('register')}}">
        <ul class="nav nav-tabs">
         <li class="nav-item">
          <a class="nav-link active_tab1" id="list_login_details" style="border:1px solid 
            #ccc;border-bottom-color: white">Login Details</a>
         </li>
         <li class="nav-item">
          <a class="nav-link inactive_tab1" id="list_personal_details" style="border:1px solid #ccc;border-bottom-color: white">Personal Details</a>
         </li>
         <li class="nav-item">
          <a class="nav-link inactive_tab1" id="list_contact_details" style="border:1px solid #ccc;border-bottom-color: white">Contact Details</a>
         </li>
        </ul>
        <div class="tab-content" style="margin-top:16px;">
         <div class="tab-pane active" id="login_details">
          <div class="panel panel-default"><br>
           <div class="panel-body">
            {{ csrf_field() }}
            <div class="form-group">
             <label>Email</label>
             <input type="text" name="email" id="email" class="form-control" />
             <span id="error_email" class="text-danger"></span>
            </div>
            <div class="form-group">
             <label>Password</label>
             <input type="password" name="password" id="password" class="form-control" />
             <span id="error_password" class="text-danger"></span>
            </div>
            <br />
            <div align="center">
             <button type="button" name="btn_login_details" id="btn_login_details" class="btn">Next</button>
            </div>
            <br />
           </div>
          </div>
         </div>
         <div class="tab-pane" id="personal_details">
          <div class="panel panel-default">
           <div class="panel-body"><br>
            <div class="form-group">
             <label>First Name</label>
             <input type="text" name="first_name" id="first_name" class="form-control" />
             <span id="error_first_name" class="text-danger"></span>
            </div>
            <div class="form-group">
             <label>Last Name</label>
             <input type="text" name="last_name" id="last_name" class="form-control" />
             <span id="error_last_name" class="text-danger"></span>
            </div>
            <div class="form-group">
             <label>Gender</label><br>
             <label class="radio-inline">
              <input type="radio" name="gender" value="Male" checked> Male
             </label>
             <label class="radio-inline">
              <input type="radio" name="gender" value="Female"> Female
             </label>
            </div>
            <br />
            <div align="center">
             <button type="button" name="previous_btn_personal_details" id="previous_btn_personal_details" class="btn btn-default">Previous
             </button>
             <button type="button" name="btn_personal_details" id="btn_personal_details" class="btn">Next</button>
            </div>
            <br />
           </div>
          </div>
         </div>
         <div class="tab-pane" id="contact_details">
          <div class="panel panel-default"><br>
           <div class="panel-body">
            <div class="form-group">
             <label>Address</label>
             <textarea name="address" id="address" class="form-control"></textarea>
             <span id="error_address" class="text-danger"></span>
            </div>
            <div class="form-group">
             <label>Mobile No.</label>
             <input type="text" name="mobile_no" id="mobile_no" class="form-control" />
             <span id="error_mobile_no" class="text-danger"></span>
            </div>
            <br />
            <div align="center">
             <button type="button" name="previous_btn_contact_details" id="previous_btn_contact_details" class="btn btn-default">Previous</button>
             <button type="button" name="btn_contact_details" id="btn_contact_details" class="btn">Register</button>
            </div>
            <br />
           </div>
          </div>
         </div>
        </div>
   </form>

      <p class="mb-0">Already have an account?
        <a href="{{ route('login') }}" class="text-center"> Login</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->

<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
  <script type="text/javascript" src="http://ajax.microsoft.com/ajax/         jQuery.Validate/1.6/jQuery.Validate.min.js"></script>
  <script type="text/javascript" src="{{asset('js/register.js')}}"></script>
</body>
</html>
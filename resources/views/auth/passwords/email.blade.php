<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('page_title', 'Admin Forgot Password')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="icon" href="{{asset('dist/img/favicon.ico')}}" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style type="text/css">
    body{font-family: Garamond;}
    .help-block p{color: red;margin-top: 10px;font-size: 0.9em;font-weight: bold;}
    p{margin-top: 10px;}
    .mb-1,.mb-0{font-weight: bold;font-size: 0.9em}
    .btn{width: 200px;border-radius: 10px;}
    .login-logo{font-weight: bolder;color: #fff;}
    .login-page{background-image: url('public/dist/img/bg-1.jpg')}
    .login-card-body{border-radius: 20px;}
    .input-group-text, .form-control{border:2px solid #ced4da;}
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{url('/')}}"><b>Forgot Password</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <h6 class="login-box-msg">Reset Password</h6>

      <form action="{{route('login')}}" method="post">
        {{ csrf_field() }}
        <div class="input-group {{$errors->has('email') ? 'has-error' : ''}} mb-3">
          <input id="email" name="email" value="{{ old('email') }}" type="email" class="form-control" placeholder="Email" autocomplete="off">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        @if($errors->has('email'))
            <span class="help-block">
              <p>{{ $errors->first('email') }}</p>
            </span>
          @endif
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <center>
            <button type="submit" class="btn btn-secondary btn-block">Send
            </button></center>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mb-0">
        <a href="{{ route('login') }}" class="text-center">Back to login</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>

</body>
</html>

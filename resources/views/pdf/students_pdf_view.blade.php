@extends('pdf.master')


@section('content')
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
          <div class="col-12">
    			<div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <h3 class="mb-2 text-dark">Total Students</h3>
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                <tr>
                  <th width="15%">Name</th>
                  <th width="25%">Email</th>
                  <th width="15%">Gender</th>
                  <th width="20%">Mobile</th>
                  <th width="15%">Status</th>
                  <th>Image</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($students as $data)
                  <tr>
                    <td>{{ $data->first_name.' '.$data->last_name }}</td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->gender }}</td>
                    <td>{{ $data->mobile }}</td>
                    <td>{{ $data->status }}</td>
                    <td>
                    	<img src="{{ asset('images/' . $data->image) }}" width="50px" height="50px">
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        </div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

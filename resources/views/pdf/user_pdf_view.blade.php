@extends('pdf.master')


@section('content')
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
          <div class="col-12">
    			<div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <h3 class="mb-2 text-dark">Total Users</h3>
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($users as $data)
                  <tr>
                    <td>{{ $data->first_name.' '.$data->last_name }}</td>
                    <td>{{ $data->email }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        </div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

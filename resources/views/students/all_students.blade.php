@extends('students.master')

@section('page_title')
    {{ "All Students List" }}
@endsection

@section('content')
<!-- <style type="text/css">
  .card-header{border-bottom:none;}
</style> -->
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12">
            <h1 class="m-0 text-dark" style="display: inline-block;">Total All Students</h1>
            <a href="{{url('student/dashboard')}}" class="btn btn-outline-dark pull-right">
              <i class="fa fa-reply"></i></a>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
          <div class="col-12">
    			<div class="card">
            <!-- /.card-header -->
            <div class="card-header">
              <!-- <a class="btn btn-warning btn-md" href="{{url('student/all_students/pdf')}}" 
                id="createNewStudent">
                  <i class="fa fa-file-pdf-o"></i>  Export PDF</a>&nbsp;
              <a class="btn btn-success btn-md" href="" 
                id="createNewStudent">
                  <i class="fa fa-file-excel-o"></i>  Export Excel</a> -->
                  
              <div class="dropdown pull-right">
                <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-download"></i>
                  Export
                </button>
                <div class="dropdown-menu dropdown-menu-right export"
                aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="{{url('student/all_students/pdf')}}">
                    <i class="fa fa-file-pdf-o"></i>PDF</a>
                  <a class="dropdown-item" href="{{url('student/all_students/excel')}}">
                    <i class="fa fa-file-excel-o"></i>Excel</a>
                  <a class="dropdown-item" href="{{url('student/all_students/csv')}}">
                    <i class="fa fa-file-csv"></i>CSV</a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <table id="example2" class="table table-bordered table-striped text-center">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Gender</th>
                  <th>Mobile</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th>Image</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($all_students as $students)
                  <tr>
                    <td>{{ $students->first_name. ' '.$students->last_name }}</td>
                    <td>{{ $students->email }}</td>
                    <td>{{ $students->gender }}</td>
                    <td>{{ $students->mobile }}</td>
                    <td>{{ $students->address }}</td>
                    <td>@if($students->status == 'Active')
                      <span class="badge badge-success badge-wide">{{ $students->status }}
                      </span>
                        @else
                        <span class="badge badge-danger badge-wide">{{ $students->status }}
                      </span>
                        @endif
                    </td>
                    <td><a class="fancybox" href="{{ asset('images/' . $students->image) }}">
                    	<img src="{{ asset('images/' . $students->image) }}" width="60px" height="60px">
                        </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        </div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@extends('students.master')

@section('page_title')
    {{ "Student Gallery" }}
@endsection

@section('content')
<style type="text/css">
    .dropzone {
        min-height: 150px;
        border: 2px dashed #343a40;
        background: #6c757d57;
        border-radius: 20px;
    }
    .dz-remove{background: #6c757d57;}
    a.dz-remove:hover{text-decoration: none;}
    #submit-all{
      background-image: linear-gradient(to right, #FF512F 0%, #DD2476 100%);
      width: 100px;
      color: white;
      border-radius: 15px;
    }
    .dz-button .fa-cloud-upload-alt{font-size: 40px;color: #007bffd1;}
    .fa-cloud-upload-alt{color: #007bffd1;}
    .panel-title{background: #aaaaaa9e;padding: 5px;border-radius: 5px;}
    .btn-link{color:#d41d2f;font-size: 18px;}
    .btn-link:hover{color:#d41d2f;}
    .btn-primary{
      color: #007bff;
      background: none;
      border: none;
      font-size: 18px;
    }
    .btn-primary:hover{
      color: #007bff;
      background: none;
      border: none;
    }
    .pagination{
      float: right;
      margin-right: 10px;
      border: 1px solid #aaa;
      margin-top: 10px;
    }
    .page-link{
      font-size: 20px;
      font-weight: bolder;
    }
    .btn-info{margin-left: 2.75rem;margin-bottom: 1.75rem;}
    .btn-secondary{margin-left: 2.75rem;margin-bottom: 1.75rem;cursor: not-allowed !important;}
</style>
  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-md-12">
            <h1 class="m-0 text-dark text-center" style="display: inline-block;">
                <i class="fa fa-images"></i>    Gallery</h1>
            <a href="{{url('student/dashboard')}}" class="btn btn-outline-dark pull-right">
              <i class="fa fa-reply"></i></a>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <form id="dropzoneForm" class="dropzone ml-2 mr-2" action="{{url('student/gallery/upload')}}">
                        @csrf
                        <div class="dz-default dz-message">
                            <button class="dz-button" type="button"><i class="fa fa-cloud-upload-alt">
                            </i><br>
                                <b>Drag & Drop Files Here</b></button>
                        </div>
                    </form>
                    <div align="center" class="mt-3">
                        <button type="button" class="btn" id="submit-all"><i class="fa fa-upload"></i>Upload</button>
                    </div>
                    <hr>
                    <div class="panel-heading">
                      <h5 class="panel-title ml-2 mr-2"><i class="fa fa-cloud-upload-alt"></i> Uploaded Images</h5>
                    </div>
                    <div class="panel-body" id="uploaded_image">
                       
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('scripts')
<script type="text/javascript">

  Dropzone.options.dropzoneForm = {

    maxFiles: 10,
    maxFilesize: 2,
    autoProcessQueue : false,
    acceptedFiles : ".png,.jpg,.gif,.jpeg",
    addRemoveLinks: true,

    init:function(){
      var submitButton = document.querySelector("#submit-all");
      myDropzone = this;

      submitButton.addEventListener('click', function(){
        myDropzone.processQueue();
      });

      this.on("complete", function(){
        if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
        {
          var _this = this;
          Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Successfull!',
                      text: 'Image Added Successfully!',
                      showConfirmButton: false,
                      timer: 1000
                    });
          _this.removeAllFiles();
          window.location.href = "{{ url('student/gallery') }}";
        }
      });

    }

  };

  $(document).on('click', '.remove_image', function(){
    var id = $(this).attr('id');
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this image!",
      icon: 'warning',
      showCancelButton: true,   
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel it!'
    })
    .then((result) => {
        if (result.isConfirmed) {
            $.ajax({                                                          
              url:"{{ url('student/gallery/delete') }}"+'/'+id,
              success:function(data){
                    Swal.fire({
                          position: 'center',
                          icon: 'success',
                          title: 'Successfull!',
                          text: 'Image Deleted Successfully!',
                          showConfirmButton: false,
                          timer: 1000
                    });
              window.location.href = "{{ url('student/gallery') }}";
              },
              error: function(){
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Something went wrong!',
                    });
              }
            });
        }
    });
  });

 
   var _token = $('input[name="_token"]').val();

   load_data('', _token);

   function load_data(id="", _token)
   {
    $.ajax({
     url:"{{ url('student/gallery/fetch') }}",
     method:"POST",
     data:{id:id, _token:_token},
     success:function(data)
     {
      $('#load_more_button').remove();
      $('#uploaded_image').append(data);
     }
    })
   }

   $(document).on('click', '#load_more_button', function(){
    var id = $(this).data('id');
    $('#load_more_button').html('<b>Loading...</b>');
    load_data(id, _token);
   });

</script>
@endsection

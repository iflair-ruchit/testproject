@extends('students.master')

@section('page_title')
    {{ "Change Student Profile" }}
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark" style="display: inline-block;">Change Profile</h1>
            <a href="{{url('student/profile')}}" class="btn btn-outline-dark pull-right">
              <i class="fa fa-reply"></i></a>
          </div><!-- /.col -->
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{url('student/profile')}}">My Profile</a></li>
               <li class="breadcrumb-item active">Change Profile</li>
            </ol>
         </div> -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-dark">
              <div class="card-header">
                <h3 class="card-title">Change Student Profile</h3>
              </div><br>
              <div class="panel-body">
      
              <form method="POST" action="{{ route('update.profile') }}" id="update_profile">
                        @csrf
                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">
                            First Name : </label>
  
                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" 
                                name="first_name" value="{{$student->first_name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">
                            Last Name : </label>
  
                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" 
                                name="last_name" value="{{$student->last_name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">
                            Email : </label>
  
                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" 
                                name="email" value="{{$student->email}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">
                            Gender : </label>
  
                            <label class="radio-inline">
                                 <input type="radio" name="gender" value="Male" {{ $student->gender == 'Male' ? 'checked' : ''}}> Male
                              </label>&nbsp;
                            <label class="radio-inline">
                                 <input type="radio" name="gender" value="Female" {{ $student->gender == 'Female' ? 'checked' : ''}}> Female
                            </label>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">
                            Address : </label>
  
                            <div class="col-md-6">
                                 <textarea name="address" id="address" class="form-control">{{$student->address}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="mobile" class="col-md-4 col-form-label text-md-right">
                            Mobile : </label>
  
                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" 
                                name="mobile" value="{{$student->mobile}}">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" name="submit" id="submit"  
                                  class="btn btn-primary">
                                    Update Profile
                                </button>
                            </div>
                        </div><br>
                </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
      $('#update_profile').validate({ // initialize the plugin
          rules: {
              'first_name': {
                  required: true
              },
              'last_name': {
                required: true
              },
              'email': {
                required: true,
                email: true
              },
              'address':{
                required: true
              },
              'mobile':{
                required: true,
                number: true,
                minlength:10,
                maxlength:10
              }
          },
          messages: {
              'first_name': {
                  required: "First Name is required"
              },
              'last_name': {
                required: "Last Name is required"
              },
              'email': {
                required: "Email is required",
                email: "Enter must be valid email address"
              },
              'address':{
                required: "Address is required"
              },
              'mobile':{
                required: "Mobile is required",
                number: "Mobile Number must be numeric",
                minlength: "Mobile Number length atleast 10 digits",
                maxlength: "Mobile Number length no more than 10 digits"
              }
          }
      });
    });
  </script>
@endsection
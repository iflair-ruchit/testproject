@extends('students.master')

@section('page_title')
    {{ "Change Password" }}
@endsection

@section('content')
<style type="text/css">
  .has-error{color: red;}
</style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark" style="display: inline-block;"><i class="fa fa-key"></i>  
              Change Password</h1>
            <a href="{{url('student/dashboard')}}" class="btn btn-outline-dark pull-right">
              <i class="fa fa-reply"></i></a>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-dark">
              <div class="card-header">
                <h3 class="card-title">Change Password</h3>
              </div><br>
              <div class="panel-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
              <form method="POST" action="{{ url('student/change_password') }}" id="change_password">
                        @csrf
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Old Password : </label>
  
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="old_password" autocomplete="old-password">
                                @error('old_password')
                                  <span class="has-error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password : </label>
  
                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm Password : </label>
    
                            <div class="col-md-6">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>
   
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" name="submit" id="submit"  
                                  class="btn btn-primary">
                                    Update Password
                                </button>
                            </div>
                        </div><br>
                </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
      $('#change_password').validate({ // initialize the plugin
          rules: {
              'old_password': {
                  required: true,
              },
              'new_password': {
                required: true,
                minlength: 8,
              },
              'new_confirm_password': {
                required: true,
                equalTo: "#new_password",
              }
          },
          messages: {
              'old_password': {
                  required: "Old Password is required"
              },
              'new_password': {
                  required: "New Password is required",
                  minlength: "New Password length atleast 8 characters"
              },
              'new_confirm_password':{
                  required: "Confirm Password is required",
                  equalTo: "New Password and Confirm Password does not match"
              }
          }
      });
    });
  </script>
@endsection
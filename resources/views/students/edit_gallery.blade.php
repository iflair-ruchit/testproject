@extends('students.master')

@section('page_title')
    {{ "Edit Gallery" }}
@endsection

@section('content')
<style type="text/css">
    .dropzone {
        min-height: 150px;
        border: 2px dashed #343a40;
        background: #6c757d57;
        border-radius: 20px;
        width: 300px;
    }
    .dz-remove{background: #6c757d57;}
    a.dz-remove:hover{text-decoration: none;}
    #submit-all{
      background-image: linear-gradient(to right, #FF512F 0%, #DD2476 100%);
      width: 100px;
      color: white;
      border-radius: 15px;
    }
    .fa-cloud-upload-alt{font-size: 40px;color: #007bffd1;}
    .panel-title{background: #aaaaaa9e;padding: 5px;border-radius: 5px;}
    .btn-link{color:#d41d2f;font-size: 18px;}
    .btn-link:hover{color:#d41d2f;}
</style>
  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-md-12">
            <h1 class="m-0 text-dark text-center" style="display: inline-block;">
                <i class="fa fa-images"></i>  Edit Gallery</h1>
            <a href="{{url('student/gallery')}}" class="btn btn-outline-dark pull-right">
              <i class="fa fa-reply"></i></a>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <center>
                    <form id="dropzoneForm" class="dropzone ml-2 mr-2" action="{{url('student/gallery/update')}}">
                        @csrf
                        <input type="hidden" name="id" id="id" value="{{$images->id}}">
                        <input type="hidden" name="user_id" id="user_id" value="{{$images->user_id}}">
                        <div class="dz-default dz-message">
                            <button class="dz-button" type="button">
                              <b><img src="{{ asset('gallery/'. $images->user_id . '/' . $images->image_name) }}" 
                              class="img-thumbnail" width="130px" height="130px" style="height:130px;"></b></button>
                        </div>

                    </form>
                    </center>
                    <div align="center" class="mt-3">
                        <button type="button" class="btn" id="submit-all"><i class="fa fa-upload"></i>Upload</button>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('scripts')
<script type="text/javascript">

   Dropzone.options.dropzoneForm = {

    maxFiles: 1,
    maxFilesize: 2,
    autoProcessQueue : false,
    acceptedFiles : ".png,.jpg,.gif,.jpeg",
    addRemoveLinks: true,

    init:function(){
      var submitButton = document.querySelector("#submit-all");
      myDropzone = this;

      submitButton.addEventListener('click', function(){
        myDropzone.processQueue();
        window.location.href = "{{ url('student/gallery') }}";
      });

      this.on("complete", function(){
        if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
        {
          var _this = this;
          Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Successfull!',
                      text: 'Image Updated Successfully!',
                      showConfirmButton: false,
                      timer: 1000
                    });
          window.location.href = "{{ url('student/gallery') }}";
          _this.removeAllFiles();
        }
      });

    }

  };

  var _token = $('input[name="_token"]').val();

   load_data('', _token);

   function load_data(id="", _token)
   {
    $.ajax({
     url:"{{ url('student/gallery/fetch') }}",
     method:"POST",
     data:{id:id, _token:_token},
     success:function(data)
     {
      $('#load_more_button').remove();
      $('#uploaded_image').append(data);
     }
    })
   }

   $(document).on('click', '#load_more_button', function(){
    var id = $(this).data('id');
    $('#load_more_button').html('<b>Loading...</b>');
    load_data(id, _token);
   });
</script>
@endsection

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('page_title', 'Student Login')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{asset('dist/img/favicon.ico')}}" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <style type="text/css">
    body{font-family: Garamond;}
    p{margin-top: 10px;}
    .mb-1,.mb-0{font-weight: bold;font-size: 0.9em}
    .btn{background-image: linear-gradient(to right, #348AC7 0%, #7474BF 100%);color: white;width: 100px;border-radius: 20px;}
    .login-page{background-image: url({{asset('dist/img/bg02.jpg')}});}
    .login-logo a{font-weight: bolder;color: #fff}
    .login-card-body{border-radius: 20px;}
    .form-control{border:1px solid #ced4da;border-radius: 20px;}
    .help-block p{color: red;font-size: 0.9em;}
    .has-error{border: 1px solid red;}
    .alert{margin-bottom: 0px;}
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo"> 
    <a href="{{url('/')}}"><b><img src="{{asset('dist/img/sms.png')}}" style="width: 300px;"></b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <h6 class="login-box-msg">
        @error('status')
            <div class="alert alert-danger">
              {{ $message }}
            </div>
        @enderror
      </h6>

      <form action="{{url('student/login')}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
             <input type="text" name="email" id="email" class="form-control" placeholder="Email" />
             <span id="error_email" class="text-danger"></span>
        </div>
        <div class="form-group">
             <input type="password" name="password" id="password" class="form-control" placeholder="Password" />
             <span id="error_password" class="text-danger"></span>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <center>
            <button type="submit" name="login_btn" id="login_btn" class="btn btn-block">Sign In</button>
            </center>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mb-1">
        <a href="{{ url('student/password/reset') }}">Forgot Password ?</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('js/login.js')}}"></script>

</body>
</html>

@extends('students.master')

@section('page_title')
    {{ "Student Profile" }}
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-md-12">
            <h1 class="m-0 text-dark text-center" style="display: inline-block;"><i class="fa fa-user">
              </i>  Student Profile</h1>
            <a href="{{url('student/dashboard')}}" class="btn btn-outline-dark pull-right">
              <i class="fa fa-reply"></i></a>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
     <center>
        <div class="card-container">
          <a class="fancybox" href="{{ asset('images/' . Auth::guard('students')->user()->image) }}">
            <img src="{{ asset('images/' . Auth::guard('students')->user()->image) }}" alt="user"/></a>
            <h3>{{Auth::guard('students')->user()->first_name . ' ' . Auth::guard('students')->user()->last_name}}</h3>
            <h5><i class="fa fa-envelope"></i>  Email : {{Auth::guard('students')->user()->email}}</h5>
            <h5>Gender : {{Auth::guard('students')->user()->gender}}</h5>
            <h5><i class="fa fa-address-card"></i>  Address : {{Auth::guard('students')->user()->address}}</h5>
            <h5><i class="fa fa-mobile"></i>  Mobile : {{Auth::guard('students')->user()->mobile}}</h5>
        </div>
        <div class="buttons">
          <a href="{{ url('/student/profile/edit') }}/{{Auth::guard('students')->user()->id}}">
            <button class="btn primary ghost"><i class="fa fa-edit"></i>Change Profile</button>
        </a>
        </div>
     </center>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
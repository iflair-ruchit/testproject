<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('page_title', 'Student Dashboard')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="icon" href="{{asset('dist/img/favicon.ico')}}" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.css" />

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
  <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.1.0/sweetalert2.min.css">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="{{asset('css/profile.css')}}">
  <style type="text/css">
    body{font-family: Garamond;}
    .btn{border-radius: 20px;}
    .badge{border-radius: 20px;}
    .img-circle{width: 30px;margin-right: 0.3rem;}
    .dropdown-toggle::after{
        margin-left: 0.5rem;
        vertical-align: 0.1rem;
    }
    .dropdown-menu{
      margin:1.7rem 0 0;
      background-color: #0d3e63;  
    }
    .navbar .dropdown .dropdown-menu:before {
      content: ' ';
      border-left: 10px solid transparent;
      border-right: 10px solid transparent;
      border-bottom: 10px solid #0d3e63;
      position: absolute;
      top: -10px;
      left: 130px;
    }
    .dropdown-item {
      padding: 0.5rem 1.5rem;
      color: #dee2e6;
    }
    .fas{margin-right: 0.3rem;}
    .navbar{background-color: #0d3e63;}
    .logo img{height: 60px;}
    .nav-link{color: rgba(255,255,255,.75);font-size: 18px !important;}
    .nav-link:hover{color: #fff;}
    .swal2-styled.swal2-cancel,.swal2-styled.swal2-confirm{border-radius: 1.25em;}
    .swal2-popup{width: 410px;}
    .swal2-styled.swal2-confirm, .swal2-styled.swal2-cancel{font-size: 14px;}
    .active{color: white;}
    .error{
      color: red;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="navbar navbar-dark navbar-expand">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="navbar-brand" href="{{url('student/dashboard')}}">
          <div class="logo">
            <img alt="logo" src="{{asset('dist/img/SMSLogo.png')}}">
          </div>
        </a>
      </li>
    </ul>

    <a href="{{url('student/dashboard')}}" class="nav-link {{ Request::is('student/dashboard') ? 'active' : '' }}"><i class="fa fa-home"></i>  Home</a>
    <a href="{{url('student/gallery')}}" class="nav-link {{ Request::is('student/gallery') ? 'active' : '' }}"><i class="fa fa-images"></i>  Gallery</a>
    <!-- <a href="{{url('student/all_students')}}" class="nav-link {{ Request::is('student/all_students') ? 'active' : '' }}"><i class="fa fa-users"></i> Students</a> -->
    
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="javascript:void(0);" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="{{ asset('images/' . Auth::guard('students')->user()->image) }}" class="img-circle elevation-2" alt="User Image">
      {{Auth::guard('students')->user()->first_name .' '. Auth::guard('students')->user()->last_name}}
            <!-- @if(Auth::guard('students')->check())
                Hello {{Auth::guard('students')->user()->first_name}}
            @elseif(Auth::guard('web')->check())
                //Hello {{Auth::guard('web')->user()->first_name}}
            @endif -->
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="{{url('student/change_password')}}"><i class="fas fa-key">
          </i>  Change Password</a>
          <a class="dropdown-item" href="{{url('student/profile')}}"><i class="fas fa-user"></i>  
          My Profile</a>
          <a class="dropdown-item" href="{{ url('student/logout') }}" onclick="confirmlogout()"><i class="fas fa-power-off"></i>  Logout</a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

@yield('content')
  
  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->

<!-- ./wrapper -->
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
  $.widget.bridge('uitooltip', $.ui.tooltip);
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.1.0/sweetalert2.min.js">
</script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script>
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
<script type="text/javascript">
  $("a.fancybox").fancybox();
</script>
<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.options =
            {
              progressBar : true,
              showMethod: 'slideDown',
              hideMethod: 'slideUp',
            }
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.options =
            {
              progressBar : true,
              showMethod: 'slideDown',
              hideMethod: 'slideUp',
            }
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.options =
            {
              progressBar : true,
              showMethod: 'slideDown',
              hideMethod: 'slideUp',
            }
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.options =
            {
              progressBar : true,
              showMethod: 'slideDown',
              hideMethod: 'slideUp',
            }
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>

<script type="text/javascript">
  function confirmlogout(){
    event.preventDefault();
    Swal.fire({
      title: 'Are you sure to logout ?',
      icon: 'warning',
      showCancelButton: true,   
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Logout!',
      cancelButtonText: 'No, Cancel!'
    })
    .then((result) => {
        if (result.isConfirmed) {
            window.location.href = "{{ url('student/logout') }}";
        }
    });
  }
</script>
@yield('scripts')
</body>
</html>

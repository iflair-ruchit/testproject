@extends('students.master')

@section('page_title')
    {{ "All Users List" }}
@endsection

@section('content')
<!-- <style type="text/css">
  .card-header{border-bottom:none;}
</style> -->
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12">
            <h1 class="m-0 text-dark" style="display: inline-block;">Total All Users</h1>
            <a href="{{url('student/dashboard')}}" class="btn btn-outline-dark pull-right">
              <i class="fa fa-reply"></i></a>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
          <div class="col-12">
    			<div class="card">
            <!-- /.card-header -->
            <div class="card-header">
              <!-- <a class="btn btn-warning btn-md" href="{{url('student/all_students/pdf')}}" 
                id="createNewStudent">
                  <i class="fa fa-file-pdf-o"></i>  Export PDF</a>&nbsp;
              <a class="btn btn-success btn-md" href="" 
                id="createNewStudent">
                  <i class="fa fa-file-excel-o"></i>  Export Excel</a> -->
                  
              <div class="dropdown pull-right">
                <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-download"></i>
                  Export
                </button>
                <div class="dropdown-menu dropdown-menu-right export"
                aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="{{url('student/all_users/pdf')}}">
                    <i class="fa fa-file-pdf-o"></i>PDF</a>
                  <!-- <a class="dropdown-item" href="{{url('student/all_students/excel')}}">
                    <i class="fa fa-file-excel-o"></i>Excel</a>
                  <a class="dropdown-item" href="{{url('student/all_students/csv')}}">
                    <i class="fa fa-file-csv"></i>CSV</a> -->
                </div>
              </div>
            </div>
            <div class="card-body">
              <table id="example2" class="table table-bordered table-striped text-center">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($all_users as $users)
                  <tr>
                    <td>{{ $users->first_name. ' '.$users->last_name }}</td>
                    <td>{{ $users->email }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        </div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@extends('students.master')

@section('page_title')
    {{ "All Deactive Students List" }}
@endsection

@section('content')

  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12">
            <h1 class="m-0 text-dark" style="display: inline-block;">Deactive Students</h1>
            <a href="{{url('student/dashboard')}}" class="btn btn-outline-dark pull-right">
              <i class="fa fa-reply"></i></a>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
          <div class="col-12">
    			<div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Gender</th>
                  <th>Mobile</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th>Image</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($deactive_students as $students)
                  <tr>
                    <td>{{ $students->first_name }}</td>
                    <td>{{ $students->email }}</td>
                    <td>{{ $students->gender }}</td>
                    <td>{{ $students->mobile }}</td>
                    <td>{{ $students->address }}</td>
                    <td><span class="badge badge-danger badge-wide">{{ $students->status }}
                        </span>
                    </td>
                    <td><a class="fancybox" href="{{ asset('images/' . $students->image) }}">
                    	<img src="{{ asset('images/' . $students->image) }}" width="60px" height="60px">
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        </div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
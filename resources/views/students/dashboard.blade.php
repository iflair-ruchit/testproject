@extends('students.master')


@section('content')
<style type="text/css">
  .bg-blue-gradient{
      background: -webkit-gradient(linear, left bottom, left top, color-stop(0, #0073b7), color-stop(1, #0089db)) !important;
      color: #fff;
    }
  .bg-green-gradient{
      background: -webkit-gradient(linear, left bottom, left top, color-stop(0, #00a65a), color-stop(1, #00ca6d)) !important;
    color: #fff;
    }
</style>
  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-md-12">
            <h1 class="m-0 text-dark"><i class="fa fa-home"></i>Dashboard</h1>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
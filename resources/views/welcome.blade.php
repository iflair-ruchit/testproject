<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('page_title', 'SMS')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="icon" href="{{asset('dist/img/favicon.ico')}}" type="image/x-icon">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="{{asset('css/welcome.css')}}">
        <style>
            body {
                font-family: 'Garamond';
                background-image: url({{asset('dist/img/bg-10.jpg')}});
            }
            .relative{
                background-image: url({{asset('dist/img/bg-10.jpg')}});
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid grid-cols-1 md:grid-cols-2">
                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-1 md:border-l text-center">
                            <div class="text-center">
                                <div class="text-lg leading-7" style="font-size: 26px;"><b>Student</b>
                                </div>
                            </div>

                            <div><br>
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-lg text-center">
                                    @if (Route::has('login'))
                                        <div>
                                            <a href="{{ url('student/login') }}" class="text-gray-900">Login</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-1 md:border-l text-center">
                            <div class="text-center">
                                <div class="text-lg leading-7" style="font-size: 26px;"><b>Admin</b></div>
                            </div>

                            <div><br>
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-lg">
                                    @if (Route::has('login'))
                                        <div>
                                            @auth
                                                <a href="{{ url('admin/dashboard') }}" class="text-gray-900">Dashboard</a>
                                            @else
                                                <a href="{{ route('login') }}" class="text-gray-900">Login</a>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

@extends('admin.master')

@section('page_title')
    {{ "Add Student" }}
@endsection

@section('content')
<style type="text/css">
   .fa{color: gray;margin-right: 5px;}
   .check{font-weight: normal;} 
   .panel-body .btn{
      background-image: linear-gradient(to right, #FF512F 0%, #DD2476 100%);
      width: 100px;
      color: white;
    }
    .nav-tabs .active{
      color:#333;
      font-weight: bold;
    }
    .nav-tabs .nav-link{color: #aaa;}
    .nav-tabs{border-bottom: 1px solid #1f2d3d;}
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
      border-color: #1f2d3d #1f2d3d #fff #1f2d3d;
    }
    .has-error{
      border-color:#cc0000;
      color: red;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<div class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="fas fa-user"></i> Add Student</h1>
         </div>
         <!-- /.col -->
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{url('admin/students')}}">Students</a></li>
               <li class="breadcrumb-item active">Add Student</li>
            </ol>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12">
            <!-- @if(session('success'))
               <div class="alert alert-success">
                 {{ session('success') }}
               </div>
               @endif -->
            <!-- general form elements -->
            <div class="card card-primary">
               <div class="card-header">
                  <h3 class="card-title">Student Details</h3>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <form action="{{url('admin/add_student')}}" method="post" enctype="multipart/form-data" id="add_form">
                  <ul class="nav nav-tabs">
                     <li class="nav-item">
                        <a href="#personal_info" class="nav-link active_tab1" id="list_personal_info" role="tab" data-toggle="tab">
                        <i class="fa fa-user"></i> Personal Info.</a>
                     </li>
                     <li class="nav-item">
                        <a href="#account_info" class="nav-link inactive_tab1" id="list_account_info" role="tab" data-toggle="tab"><i class="fa fa-lock"></i> Account Info.</a>
                     </li>
                     <li class="nav-item">
                        <a href="#contact_info" class="nav-link inactive_tab1" id="list_contact_info" role="tab" data-toggle="tab"><i class="fa fa-mobile"></i> Contact Info.</a>
                     </li>
                     <li class="nav-item">
                        <a href="#other_info" class="nav-link inactive_tab1" id="list_other_info" role="tab" data-toggle="tab"><i class="fa fa-image"></i> Other Info.</a>
                     </li>
                  </ul>
                  <div class="tab-content">
                     <div role="tabpanel" class="tab-pane active" id="personal_info">
                        <div class="panel panel-default">
                           <br>
                           <div class="panel-body">
                              {{ csrf_field() }}
                              <input type="hidden" name="id" id="id">
                              <div class="form-group">
                                 <label>First Name <span class="required">*</span></label>
                                 <input type="text" name="first_name" id="first_name" class="form-control" autocomplete="off" placeholder="Enter First Name" value="{{old('first_name')}}" />
                                 <span id="error_first_name" class="has-error"></span>
                              </div>
                              <div class="form-group">
                                 <label>Last Name <span class="required">*</span></label>
                                 <input type="text" name="last_name" id="last_name" class="form-control" autocomplete="off" placeholder="Enter Last Name" value="{{old('last_name')}}" />
                                 <span id="error_last_name" class="has-error"></span>
                              </div>
                              <div class="form-group">
                                 <label>Gender <span class="required">*</span></label><br>
                                 <label class="radio-inline">
                                 <input type="radio" name="gender" value="Male" checked> Male
                                 </label>&nbsp;
                                 <label class="radio-inline">
                                 <input type="radio" name="gender" value="Female"> Female
                                 </label><br>
                                 <span id="error_gender" class="has-error"></span>
                              </div>
                              </button>
                              <button type="button" name="btn_personal_info" id="btn_personal_info" class="btn btn-md pull-right">Next
                              </button>
                              <br />
                           </div>
                        </div>
                     </div>
                     <div role="tabpanel" class="tab-pane" id="account_info">
                        <div class="panel panel-default">
                           <br>
                           <div class="panel-body">
                              <div class="form-group">
                                 <label>Email <span class="required">*</span></label>
                                 <input type="text" name="email" id="email" class="form-control" autocomplete="off" placeholder="Enter Email" value="{{old('email')}}"/>
                                 <span id="error_email" class="has-error"></span>
                              </div>
                              <div class="form-group">
                                 <label>Password <span class="required">*</span></label>
                                 <input type="password" name="password" id="password" 
                                    class="form-control" placeholder="Enter password"/>
                                  <span id="error_password" class="has-error"></span>
                              </div>
                              <button type="button" name="previous_btn_account_info" 
                                id="previous_btn_account_info" class="btn btn-md">Previous
                              </button>
                              <button type="button" name="btn_account_info" id="btn_account_info" class="btn btn-md pull-right">Next
                              </button>
                              <br />
                           </div>
                        </div>
                     </div>
                     <div role="tabpanel" class="tab-pane" id="contact_info">
                        <div class="panel panel-default">
                           <br>
                           <div class="panel-body">
                              <div class="form-group">
                                 <label>Address <span class="required">*</span></label>
                                 <textarea name="address" id="address" class="form-control" autocomplete="off" placeholder="Enter Address" onkeypress="showLocation()">{{old('address')}}</textarea>
                                 <!-- <input type="text" name="address" id="address" class="form-control" autocomplete="off" placeholder="Enter Address" onkeypress="showLocation()" value="{{old('address')}}" /> -->
                                 <input type="hidden" name="latitude" id="latitude">
                                 <input type="hidden" name="longitude" id="longitude">
                                 <span id="error_address" class="has-error"></span>
                              </div>
                              <div class="form-group">
                                 <label>Mobile No. <span class="required">*</span></label>
                                 <input type="text" name="mobile" id="mobile" class="form-control" autocomplete="off" placeholder="Enter Mobile Number" value="{{old('mobile')}}" />
                                 <span id="error_mobile" class="has-error"></span>
                              </div>
                              <button type="button" name="previous_btn_contact_info" 
                              id="previous_btn_contact_info" class="btn btn-md"> Previous
                              </button>
                              <button type="button" name="btn_contact_info" id="btn_contact_info" class="btn btn-md pull-right"> Next
                              </button>
                              <br />
                           </div>
                        </div>
                     </div>
                     <div role="tabpanel" class="tab-pane" id="other_info">
                        <div class="panel panel-default">
                           <br>
                           <div class="panel-body">
                              <div class="form-group" id="skills">
                                 <label>Skills <span class="required">*</span></label><br>
                                 <label>
                                 <input class="chek" type="checkbox" name="skills[]" value="PHP" id="skills">
                                 &nbsp; PHP
                                 </label>
                                 <label>&nbsp;
                                 <input class="chek" type="checkbox" name="skills[]" value="Laravel" id="skills">
                                 &nbsp; Laravel
                                 </label>
                                 <label>&nbsp;
                                 <input class="chek" type="checkbox" name="skills[]" value=
                                 "Codeigniter" id="skills">&nbsp; Codeigniter
                                 </label>
                                 <label>&nbsp;
                                 <input class="chek" type="checkbox" name="skills[]" value=
                                 "Wordpress" id="skills">&nbsp; Wordpress
                                 </label>
                                 <label>&nbsp;
                                 <input class="chek" type="checkbox" name="skills[]" value=
                                 "Angular JS" id="skills">&nbsp; Angular JS
                                 </label><br>
                                 <span id='error_skills' class="has-error"><span>
                              </div>
                              <div class="form-group">
                                 <label>Status <span class="required">*</span></label><br>
                                 <label class="radio-inline">
                                 <input type="radio" name="status" value="Active" checked> Active
                                 </label>&nbsp;
                                 <label class="radio-inline">
                                 <input type="radio" name="status" value="Deactive"> Deactive
                                 </label>
                                 <span id="error_status" class="has-error"></span>
                              </div>
                              <label for="image">Image</label>  <span class="required">*</span>
                              <div class="custom-file">
                                 <input type="file" class="custom-file-input" id="image" 
                                    name="image">
                                 <label class="custom-file-label" for="customFile">Choose file</label>
                                 <!-- @error('image')
                                  <span id="error_image" class="text-danger">{{ $message }}</span>
                                 @enderror -->
                                 <span id='error_image' class="has-error"><span>
                              </div><br><br>
                              <button type="button" name="previous_btn_other_info" 
                              id="previous_btn_other_info" class="btn btn-md"> Previous
                              </button>
                              <button type="submit" name="btn_other_info" id="btn_other_info"
                                  class="btn btn-md pull-right"> Submit
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  </form>
               </div>
            </div>
            <!-- /.card -->
         </div>
      </div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!--  <a id="back-to-top" href="#" class="btn btn-secondary back-to-top" role="button" aria-label="Scroll to top">
   <i class="fas fa-chevron-up"></i>
   </a> -->
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF_rD5BqZnkS3-qwxezgwNr3EDGRnGHmA&libraries=places" async defer></script>
<script type="text/javascript" src="{{asset('js/add_student.js')}}"></script>
<script type="text/javascript">

    function showLocation() {
            var input = document.getElementById('address');         
            var autocomplete = new google.maps.places.Autocomplete(input, {
                types: ["geocode"]
            });

            google.maps.event.addListener(autocomplete, 'place_changed', function() {

              var place = autocomplete.getPlace();
              var lat = place.geometry.location.lat();
              var lng = place.geometry.location.lng();
              $("#latitude").val(lat);
              $("#longitude").val(lng);
            });
        }
</script>
<script type="text/javascript">
   $(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $("#add_form").on('submit',function(event){
         event.preventDefault();
            $.ajax({
                    url:"{{ url('admin/add_student') }}",
                    method:"POST",
                    data: $('#add_form').serialize(),
                    contentType: false.
                    cache: false,
                    processData: false,
                    dataType: "json",
                    success:function(data)
                    {
                        window.location.href = "{{ url('admin/students') }}";
                    }
            });
      });
   });
</script>
@endsection

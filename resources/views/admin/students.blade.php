@extends('admin.master')

@section('page_title')
    {{ "Students" }}
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="fa fa-users"></i> Students</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Students</li>
            </ol>
          </div>
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
          <div class="col-12">
            @if(session('status'))
            <div class="alert alert-danger">
              {{ session('status') }}
            </div>
            @endif
            @if(session('success'))
            <div class="alert alert-success">
              {{ session('success') }}
            </div>
            @endif
    			<div class="card">
            <div class="card-header">
                <a class="btn btn-success btn-sm" href="{{url('admin/add_student')}}" id="createNewStudent">
                  <i class="fa fa-plus"></i>  Add</a>
                <a class="btn btn-warning btn-sm" href="{{url('admin/student/import_csv')}}" id="import_csv">
                  <i class="fa fa-download"></i>  Import</a>
                  <button type="button" name="bulk_delete" id="bulk_delete" class="btn btn-danger btn-sm pull-right"><i class="fa fa-trash"></i> Delete All</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                   <th>Name</th>
                   <th>Email</th>
                   <th>Gender</th>
                   <th>Mobile</th>
                   <th>Skills</th>
                   <th>Status</th>
                   <th>Image</th>
                   <th width="12%">Action</th>
                   <!-- <th><button type="button" name="bulk_delete" id="bulk_delete" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></th> -->
                   <th><input type="checkbox" id="master"></th>
                  </tr>
                </thead>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        </div>
    	</div>
    </section>
    <!-- /.content -->
  </div>

  <button type="button" class="btn btn-primary" id="OpenStatusModel" data-toggle="modal" data-target="#Confirmstatus" style="display: none">Launch demo modal</button>

  <div class="modal fade" id="Confirmstatus" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="fa fa-close"></i>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to change status for this Student?</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary font-weight-bold" id="confirmChangeStatus">Yes</button>
                </div>
            </div>
        </div>
    </div>
  <!-- /.content-wrapper -->
@endsection
@section('scripts')
  <script type="text/javascript">
  $(function () {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    //Student list view with Serverside datatables

    var table = $('.data-table').DataTable({
        "order": [[ 0, "desc" ]],
        processing: true,
        serverSide: true,
        ajax: "{{ url('admin/students') }}",
        columns: [
            {data: 'first_name', name: 'first_name'},
            {data: 'email', name: 'email'},
            {data: 'gender', name: 'gender', orderable: false},
            {data: 'mobile', name: 'mobile', orderable: false},
            {data: 'skills', name: 'skills', orderable: false},
            { data: 'status', name:'status', orderable: false,
                "render": function(status, type,row){
                    if(status=='Active'){
                        
                        return '<a href="#" data-id="'+row.id+'" data-status="Deactive" class="change_status"><span class="badge badge-success badge-wide">Active</span></a>';
                    } else {
                       
                        return '<a href="#" data-id="'+row.id+'" data-status="Active" class="change_status"><span class="badge badge-danger badge-wide">Deactive</span></a>'
                    }
                },
            },
            {
              data: 'image', 
              name: 'image',
              render: function (data, type, full, meta) {
                  return "<img src = {{asset('images/')}}/" + data + " width = '80' class = 'img-thumbnail'/>";
                },
              orderable:false
            },
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false}
        ]
    });

    $('#master').on('click', function(e) {
      if($(this).is(':checked',true))
      {
        $(".student_checkbox").prop('checked', true);
      } else {
        $(".student_checkbox").prop('checked',false);
      }
    });

    //Change Status for Stduent

    $(document).on('click','.change_status', function(e){
    e.preventDefault();
    var currentPointer = jQuery(this);
    var id = currentPointer.attr('data-id');
    var status = currentPointer.attr('data-status');
    dataToSend = {};
    dataToSend["id"] = id;
    dataToSend["status"] = status;

    var urlRoute = "{{ route('students.changeStatus') }}/"+id+'/'+status;

    $('#OpenStatusModel').trigger('click');

    $('#confirmChangeStatus').on('click', function(e) {
    $("#Confirmstatus").modal("hide");
    jQuery.ajax({
            url: urlRoute,
            type:'POST',
            headers:{ 'X-CSRF-Token' : "{{ csrf_token() }}" },
            data :  dataToSend,
            success: function(data)
            {  
                var msg = data.msg;
                if(data.status == 'success')
                {
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            timeOut: 4000
                        };
                        toastr.success(msg);
                    }, 1300);
                    $('.data-table').DataTable().ajax.reload();
                }
                else
                {
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            timeOut: 4000
                        };
                        toastr.error(msg);
                    }, 1300);
                }

                t.draw();

              }
          });
       });
    });

    //Delete student with id

    $(document).on('click', '.delete', function(){
    var id = $(this).attr('id');
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this student !",
      icon: 'warning',
      showCancelButton: true,   
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel it!'
    })
    .then((result) => {
        if (result.isConfirmed) {
            $.ajax({                                                          
              url:"{{ url('admin/students/destroy') }}"+'/'+id,
              success: function (data) {
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Successfull!',
                  text: 'Data Deleted Successfully!',
                  showConfirmButton: false,
                  timer: 2000
                })
                $('.data-table').DataTable().ajax.reload();
              },
              error: function(){
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Something went wrong!',
                    });
              }
            });
        }
      });
    });

    //Bult Data delete of students

    /*$(document).on('click', '#bulk_delete', function(){
        var id = [];
        if(confirm("Are you sure you want to Delete this data?"))
        {
            $('.student_checkbox:checked').each(function(){
                id.push($(this).val());
            });
            if(id.length > 0)
            {
                var join_id = id.join(",");

                $.ajax({
                    url:"{{ url('admin/students/bulkremove')}}",
                    method:"post",
                    data:'id='+join_id,
                    success:function(data)
                    {
                        Swal.fire({
                          position: 'center',
                          icon: 'success',
                          title: 'Successfull!',
                          text: 'Data Deleted Successfully!',
                          showConfirmButton: false,
                          timer: 2000
                        })
                        $('.data-table').DataTable().ajax.reload();
                    }
                });
            }
            else
            {
                Swal.fire({
                  icon: 'warning',
                  title: 'Sorry...',
                  text: 'Please select atleast one checkbox!',
                })
            }
        }
    });*/
    $(document).on('click','#bulk_delete',function(){
    var id = [];
    var $this = $(this);
    var currentPointer = jQuery(this);
    var userId = currentPointer.attr('id');
    $('.student_checkbox:checked').each(function(){
            id.push($(this).val());
        });
        if(id.length > 0)
        {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this student !",
          icon: 'warning',
          showCancelButton: true,   
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel it!'
          })
          .then((result) => {
            if (result.isConfirmed) {
              var join_id = id.join(",");
            $.ajax
            ({
                url:"{{ url('admin/students/bulkremove')}}",
                method:"post",
                headers:{ 'X-CSRF-Token' : "{{ csrf_token() }}" },
                data:'id='+join_id,
                success:function(data)
                {
                    Swal.fire({
                          position: 'center',
                          icon: 'success',
                          title: 'Successfull!',
                          text: 'Data Deleted Successfully!',
                          timer: 2000
                        })
                    $('.data-table').DataTable().ajax.reload();
                }
            });
          }
         });
        }
        else
        {
           Swal.fire({
                  icon: 'warning',
                  title: 'Sorry...',
                  text: 'Please select atleast one checkbox!',
                })
        }
    
    return false;
  });
  });
</script>
@endsection
@extends('admin.master')

@section('page_title')
    {{ "Admin Users" }}
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Admin Users</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Admin Users</li>
            </ol>
          </div>
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
          <div class="col-12">
    			<div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                   <th>Name</th>
                   <th>Email</th>
                   <th>Mobile</th>
                   <th>Address</th>
                   <th width="10%">Action</th>
                   <th><button type="button" name="bulk_delete" id="bulk_delete" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></th>
                  </tr>
                </thead>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        </div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('scripts')
  <script type="text/javascript">
  $(function () {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('admin/all_admins') }}",
        columns: [
            {data: 'first_name', name: 'first_name'},
            {data: 'email', name: 'email'},
            {data: 'mobile_no', name: 'mobile_no', orderable: false},
            {data: 'address', name: 'address', orderable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false}
        ]
    });

    $(document).on('click', '.delete', function(){
    var id = $(this).attr('id');
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this student !",
      icon: 'warning',
      showCancelButton: true,   
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel it!'
    })
    .then((result) => {
        if (result.isConfirmed) {
            $.ajax({                                                          
              url:"{{ url('admin/all_admins/destroy') }}"+'/'+id,
              success: function (data) {
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Successfull!',
                  text: 'Data Deleted Successfully!',
                  showConfirmButton: false,
                  timer: 2000
                })
                $('.data-table').DataTable().ajax.reload();
              },
              error: function(){
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Something went wrong!',
                    });
              }
            });
        }
      });
    });

    $(document).on('click', '#bulk_delete', function(){
        var id = [];
        if(confirm("Are you sure you want to Delete this data?"))
        {
            $('.student_checkbox:checked').each(function(){
                id.push($(this).val());
            });
            if(id.length > 0)
            {
                var join_id = id.join(",");

                $.ajax({
                    url:"{{ url('admin/all_admins/bulkremove')}}",
                    method:"get",
                    data:'id='+join_id,
                    success:function(data)
                    {
                        Swal.fire({
                          position: 'center',
                          icon: 'success',
                          title: 'Successfull!',
                          text: 'Data Deleted Successfully!',
                          showConfirmButton: false,
                          timer: 2000
                        })
                        $('.data-table').DataTable().ajax.reload();
                    }
                });
            }
            else
            {
                Swal.fire({
                  icon: 'warning',
                  title: 'Sorry...',
                  text: 'Please select atleast one checkbox!',
                })
            }
        }
    });
  });
</script>
@endsection
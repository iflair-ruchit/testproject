@extends('admin.master')

@section('page_title')
    {{ "Import CSV" }}
@endsection

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><i class="fa fa-download"></i> Import File</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/students') }}">Students</a></li>
              <li class="breadcrumb-item active">Import File</li>
            </ol>
          </div>
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
          <div class="col-12">
            @if(session('status'))
            <div class="alert alert-danger">
              {{ session('status') }}
            </div>
            @endif
            @if(session('success'))
            <div class="alert alert-success">
              {{ session('success') }}
            </div>
            @endif
    			<div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <form action="{{ url('admin/student/import') }}" method="POST" 
                  enctype="multipart/form-data" id="import">
                @csrf
                <div class="custom-file has-error">
                    <input type="file" class="custom-file-input" id="file" name="file" accept=".csv">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                @if($errors->has('file'))
                    <span class="text-danger">
                      <p>{{ $errors->first('file') }}</p>
                    </span>
                @endif
                <button class="btn btn-success mt-3" name="import">Import</button><br><br>
                <p>(e.g. students.csv)</p>
                 <a href="{{asset('import/students.csv')}}" download="students.csv">
                  <i class="fa fa-download"></i>  Download 
                 </a> ( .csv File )
            </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        </div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('scripts')
  <script type="text/javascript">
      $(document).ready(function(){
            $("#import").validate({
                rules: {
                    file: {
                        required: true,
                        extension: 'csv',
                        accept : 'text/csv,text/comma-separated-value,application/vnd.ms-excel,application/vnd.msexcel,application/csv'
                    }
                },
                messages:{
                    file: {
                        required: ".CSV File is required",
                        accept : "The file type must be 'CSV'."
                      } 
                }
            });
        });
    </script>
@endsection
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*-------------------------------------------------------------*/

// Admin Side Authentication

/*-------------------------------------------------------------*/

Auth::routes();

Route::get('admin/dashboard', 'AdminController@index')->name('home');
Route::get('/logout', 'AdminController@logout');

/*-------------------------------------------------------------*/

//Admin Side All Admins

/*-------------------------------------------------------------*/

Route::get('admin/all_admins','AdminController@all_admins');
Route::get('admin/all_admins/destroy/{id}', 'AdminController@destroy');
Route::get('admin/all_admins/bulkremove', 'AdminController@bulkremove');

/*-------------------------------------------------------------*/

// Admin Side Students

/*-------------------------------------------------------------*/

Route::resource('admin/students','StudentController');
Route::get('admin/add_student',function(){
	return view('admin.add_student');
});

Route::post('admin/add_student','StudentController@store');
Route::get('admin/edit_student/{id}', 'StudentController@edit');
Route::post('admin/edit_student','StudentController@update');
Route::get('admin/students/destroy/{id}', 'StudentController@destroy');
Route::post('admin/students/bulkremove', 'StudentController@bulkremove');
Route::post('admin/students/status/{id?}/{status?}', 'StudentController@changeStatus')	
					->name("students.changeStatus");
Route::get('admin/student/import_csv',function(){
	return view('admin.import');
});
Route::post('admin/student/import','StudentController@import');
/*-------------------------------------------------------------*/

//Admin Change Password

/*-------------------------------------------------------------*/

Route::get('admin/change_password', 'AdminController@showChangePasswordForm');
Route::post('admin/change_password', 'AdminController@changePassword')
				->name('change.password');

/*-------------------------------------------------------------*/

//Admin Change Profile

/*-------------------------------------------------------------*/

/*Route::get('admin/profile', 'AdminController@show_profile');
Route::get('admin/profile/edit/{id}', 'AdminController@edit_profile');
Route::post('admin/profile/edit', 'AdminController@update_profile');*/

/*-------------------------------------------------------------*/

//User Side Authentication

/*-------------------------------------------------------------*/

Route::group(['middleware' => ['students']], function () {
    Route::get('student/dashboard', 'FrontController@index');
});
//Route::get('student/dashboard', 'FrontController@index');
Route::get('student/login', 'FrontController@initContent');
Route::post('student/login', 'FrontController@login');
Route::get('student/logout', 'FrontController@logout');

Route::get('student/password/reset','Auth\ForgotPasswordController@showLinkRequestForm')
			->name('password.request');
			
Route::post('student/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')
    		->name('password.email');

Route::get('student/password/reset/{token}','Auth\ResetPasswordController@showResetForm')
    		->name('password.reset');

Route::post('student/password/reset','Auth\ResetPasswordController@reset')
    		->name('password.update');

/*-------------------------------------------------------------*/

//Student Change Password

/*-------------------------------------------------------------*/

Route::get('student/change_password', 'ChangePasswordController@index');
Route::post('student/change_password', 'ChangePasswordController@changePassword');

/*-------------------------------------------------------------*/

//Student Change Profile

/*-------------------------------------------------------------*/

Route::get('student/profile', 'StudentController@show_profile');
Route::get('student/profile/edit/{id}', 'StudentController@edit_profile');
Route::post('student/profile/edit', 'StudentController@update_profile')->name('update.profile');

/*-------------------------------------------------------------*/

//Students Data Show in Table (Active / Deactive)

/*-------------------------------------------------------------*/

Route::get('student/all_students','StudentController@show');
Route::get('student/all_users','StudentController@show_user');
Route::get('student/active_students','StudentController@show_active');
Route::get('student/deactive_students','StudentController@show_deactive');

/*-------------------------------------------------------------*/

//Data Export to Documents

/*-------------------------------------------------------------*/

Route::get('student/all_students/pdf','StudentController@exportPDF');
Route::get('student/all_users/pdf','StudentController@exportPDF_users');
Route::get('student/all_students/excel','StudentController@exportExcel');
Route::get('student/all_students/csv','StudentController@exportCSV');

/*-------------------------------------------------------------*/

//Multiple Image Upload with Dropzone

/*-------------------------------------------------------------*/

Route::get('student/gallery', 'DropzoneController@index');
Route::post('student/gallery/upload', 'DropzoneController@upload');
//Route::get('student/gallery/fetch', 'DropzoneController@fetch');
Route::post('student/gallery/fetch', 'DropzoneController@fetch');
Route::get('student/gallery/delete/{id}', 'DropzoneController@delete');
Route::get('student/gallery/edit/{user_id}/{id}', 'DropzoneController@edit');
Route::post('student/gallery/update', 'DropzoneController@update');

/*-------------------------------------------------------------*/

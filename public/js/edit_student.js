$(document).ready(function() {
    $('#btn_personal_info').click(function() {

        var error_first_name = '';
        var error_last_name = '';
        var error_gender = '';

        if ($.trim($('#first_name').val()).length == 0) {
            error_first_name = 'First Name is required';
            $('#error_first_name').text(error_first_name);
            $('#first_name').addClass('has-error');
        } else {
            error_first_name = '';
            $('#error_first_name').text(error_first_name);
            $('#first_name').removeClass('has-error');
        }

        if ($.trim($('#last_name').val()).length == 0) {
            error_last_name = 'Last Name is required';
            $('#error_last_name').text(error_last_name);
            $('#last_name').addClass('has-error');
        } else {
            error_last_name = '';
            $('#error_last_name').text(error_last_name);
            $('#last_name').removeClass('has-error');
        }

        if (error_first_name != '' || error_last_name != '') {
            return false;
        } else {
            $('#btn_personal_info').removeClass('active active_tab1');
            $('#btn_personal_info').removeAttr('href data-toggle');
            $('#personal_info').removeClass('active');
            $('#btn_personal_info').addClass('inactive_tab1');
            $('#list_account_info').removeClass('inactive_tab1');
            $('#list_account_info').addClass('active_tab1 active');
            $('#list_account_info').attr('href', '#account_info');
            $('#list_account_info').attr('data-toggle', 'tab');
            $('#account_info').addClass('active in');
        }
    });

    $('#previous_btn_account_info').click(function() {
        $('#list_account_info').removeClass('active active_tab1');
        $('#list_account_info').removeAttr('href data-toggle');
        $('#account_info').removeClass('active in');
        $('#list_account_info').addClass('inactive_tab1');
        $('#list_personal_info').removeClass('inactive_tab1');
        $('#list_personal_info').addClass('active_tab1 active');
        $('#list_personal_info').attr('href', '#personal_info');
        $('#list_personal_info').attr('data-toggle', 'tab');
        $('#personal_info').addClass('active in');
    });

    $('#btn_account_info').click(function() {
        var error_email = '';
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if ($.trim($('#email').val()).length == 0) {
            error_email = 'Email is required';
            $('#error_email').text(error_email);
            $('#email').addClass('has-error');
        } else {
            if (!filter.test($('#email').val())) {
                error_email = 'Invalid Email';
                $('#error_email').text(error_email);
                $('#email').addClass('has-error');
            } else {
                error_email = '';
                $('#error_email').text(error_email);
                $('#email').removeClass('has-error');
            }
        }

        if (error_email != '') {
            return false;
        } else {
            $('#list_account_info').removeClass('active active_tab1');
            $('#list_account_info').removeAttr('href data-toggle');
            $('#account_info').removeClass('active');
            $('#list_account_info').addClass('inactive_tab1');
            $('#list_contact_info').removeClass('inactive_tab1');
            $('#list_contact_info').addClass('active_tab1 active');
            $('#list_contact_info').attr('href', '#contact_info');
            $('#list_contact_info').attr('data-toggle', 'tab');
            $('#contact_info').addClass('active in');
        }
    });

    $('#previous_btn_contact_info').click(function() {
        $('#list_contact_info').removeClass('active active_tab1');
        $('#list_contact_info').removeAttr('href data-toggle');
        $('#contact_info').removeClass('active in');
        $('#list_contact_info').addClass('inactive_tab1');
        $('#list_account_info').removeClass('inactive_tab1');
        $('#list_account_info').addClass('active_tab1 active');
        $('#list_account_info').attr('href', '#account_info');
        $('#list_account_info').attr('data-toggle', 'tab');
        $('#account_info').addClass('active in');
    });

    $('#btn_contact_info').click(function() {
        var error_address = '';
        var error_mobile = '';
        var mobile_validation = /^\d{10}$/;
        if ($.trim($('#address').val()).length == 0) {
            error_address = 'Address is required';
            $('#error_address').text(error_address);
            $('#address').addClass('has-error');
        } else {
            error_address = '';
            $('#error_address').text(error_address);
            $('#address').removeClass('has-error');
        }

        if ($.trim($('#mobile').val()).length == 0) {
            error_mobile = 'Mobile Number is required';
            $('#error_mobile').text(error_mobile);
            $('#mobile').addClass('has-error');
        } else {
            if (!mobile_validation.test($('#mobile').val())) {
                error_mobile = 'Invalid Mobile Number';
                $('#error_mobile').text(error_mobile);
                $('#mobile').addClass('has-error');
            } else {
                error_mobile = '';
                $('#error_mobile').text(error_mobile);
                $('#mobile').removeClass('has-error');
            }
        }
        
        if (error_address != '' || error_mobile != '') {
            return false;
        } else {
            $('#list_contact_info').removeClass('active active_tab1');
            $('#list_contact_info').removeAttr('href data-toggle');
            $('#contact_info').removeClass('active');
            $('#list_contact_info').addClass('inactive_tab1');
            $('#list_other_info').removeClass('inactive_tab1');
            $('#list_other_info').addClass('active_tab1 active');
            $('#list_other_info').attr('href', '#other_info');
            $('#list_other_info').attr('data-toggle', 'tab');
            $('#other_info').addClass('active in');
        }

      });

      $('#previous_btn_other_info').click(function() {
        $('#list_other_info').removeClass('active active_tab1');
        $('#list_other_info').removeAttr('href data-toggle');
        $('#other_info').removeClass('active in');
        $('#list_other_info').addClass('inactive_tab1');
        $('#list_contact_info').removeClass('inactive_tab1');
        $('#list_contact_info').addClass('active_tab1 active');
        $('#list_contact_info').attr('href', '#contact_info');
        $('#list_contact_info').attr('data-toggle', 'tab');
        $('#contact_info').addClass('active in');
      });

       $('#btn_other_info').click(function() {
            
            $("#add_form").submit();
      });   
});
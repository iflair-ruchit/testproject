<?php

namespace App\Imports;

use App\Student;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class StudentImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //dd($row);
        return new Student([
            'first_name'     => $row['first_name'],
            'last_name'     => $row['last_name'],
            'email'      => $row['email'], 
            'password' => Hash::make($row['password']),
            'token' => Str::random(32),
            'gender' => $row['gender'],
            'address' => $row['address'],
            'mobile' => $row['mobile'],
            'skills' => $row['skills'],
            'status' => $row['status'],
            'image' => $row['image'],
        ]);

    }
}

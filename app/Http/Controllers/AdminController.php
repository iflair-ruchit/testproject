<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Student;
use Brian2694\Toastr\Facades\Toastr;
use Auth;
use PDF;
use DataTables;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $admin_users = User::all()->count();
        $students = Student::all()->count();
        $active = Student::where('status', 'Active')->count();
        $deactive = Student::where('status', 'Deactive')->count();
        return view('admin.dashboard',compact('admin_users','students','active','deactive'));
    }

    public function all_admins(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    
                    $button = '<button type="button" name="delete" id="'.$data->id.'" 
                    class="delete btn btn-danger btn-sm"><i class="fa fa-trash"></i>   </button>';
                        return $button;
                    })
                ->addColumn('checkbox', '<input type="checkbox" name="student_checkbox[]" class="student_checkbox" value="{{$id}}" />')
                ->rawColumns(['checkbox','action'])
                ->make(true);
        }

        return view('admin.all_admins');
    }

    public function initContent()
    {
       return view('auth.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function showChangePasswordForm()
    {

      return view('admin.change_password');
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'old_password' => ['required', new MatchOldPassword],
            'new_password' => 'required|min:6|different:old_password',
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        $notification = array(
            'message' => 'Password successfully changed!', 
            'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();
    }

    public function bulkremove(Request $request)
    {
        $admin_id = $request->input('id');
        $admin = User::whereIn('id', explode(",", $admin_id));
        $admin->delete();
    }

}

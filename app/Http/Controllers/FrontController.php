<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;
use App\Student;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class FrontController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'student/dashboard';

    protected $logoutRedirectTo = 'student/login';

    protected $guard = 'students';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('students')->except('logout');
    }

    public function initContent()
    {
       return view('students.login');
    }


    public function index()
    {
        if ($this->guard()->check()) {
            $count = Student::all()->count();
            $active = Student::where('status', 'Active')->count();
            $deactive = Student::where('status', 'Deactive')->count();
            
            return view('students.dashboard',compact('count','active','deactive'));
        }
        else{
            return redirect('student/login');
        }
        // return view('students.dashboard');
    }
    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('students');
    }
    
    public function login(Request $request)
    {
      $this->validate($request, [
        'email' => 'required|email',
        'password' => 'required'
      ]);

      if (Auth::guard('students')->attempt(['email' => $request->email, 'password' => $request->password,'status' => 'Active'])) {
        return redirect()->intended($this->redirectTo);
      }
      return redirect()->back()
                ->withInput($request->only('email'))
                ->withErrors([
                    'status' => 'This account will be not active'
                ]);
    }
    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::guard('students')->logout();
        return redirect($this->logoutRedirectTo);
    }

    public function password_reset()
    {
        return view('students.email');
    }    
}    
<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use Auth;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Hash;
use Brian2694\Toastr\Facades\Toastr;
use DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\StudentExport;
use App\Imports\StudentImport;
use Cviebrock\EloquentSluggable\Services\SlugService;


class StudentController extends Controller
{
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Student::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {

                    $button = '<a href = "edit_student/'.$data->id.'"><button type="button" name="edit" id="'
                        .$data->id.'" 
                    class="edit btn btn-primary btn-sm"><i class="fa fa-edit"></i>
                    </button></a>';

                    $button .= '&nbsp;&nbsp;';
                    
                    $button .= '<button type="button" name="delete" id="'.$data->id.'" 
                    class="delete btn btn-danger btn-sm"><i class="fa fa-trash"></i>   </button>';
                        return $button;
                    })
                ->addColumn('checkbox', '<input type="checkbox" name="student_checkbox[]" class="student_checkbox" value="{{$id}}" />')
                ->rawColumns(['checkbox','action'])
                ->make(true);
        }

        return view('admin.students');
    }

    
    public function create()
    {
        
    }

    
    public function store(Request $request)
    {
            $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'skills' => 'required',
            'status' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg|max:2048',
            ]);

            $image = $request->file('image');
            $image_name = 'image_'.rand().'.'.$image->getClientOriginalExtension();
            $destination = public_path('images');
            $image->move($destination,$image_name);

            $form_data = array(
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'slug' => SlugService::createSlug(Student::class, 'slug', $request->first_name),
                'gender' => $request->gender,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'token' =>  Str::random(32), 
                'address' => $request->address,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'mobile' => $request->mobile,
                'skills' => implode(',',$request->skills),
                'status' => $request->status,
                'image' => $image_name
            );

            //dd($form_data);

            Student::create($form_data);
            $notification = array(
                'message' => 'Student Successfully Added!', 
                'alert-type' => 'success'
            );
            
            return redirect('admin/students')->with($notification);
    }

    public function edit($id)
    {
        $student = Student::find($id);   
        return view('admin.edit_student',compact('student'));        
    }

    public function update(Request $request)
    {
         $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'email' => 'required|email',
            //'password' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'skills' => 'required',
            'status' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg|max:2048',
            ]);

            $image = $request->file('image');
            $image_name = 'image_'.rand().'.'.$image->getClientOriginalExtension();
            $destination = public_path('images');
            $image->move($destination,$image_name);

            $form_data = array(
                'id' => $request->id,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'slug' => SlugService::createSlug(Student::class, 'slug', $request->first_name),
                'gender' => $request->gender,
                'email' => $request->email,
                //'password' => Hash::make($request->password),
                'address' => $request->address,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'mobile' => $request->mobile,
                'skills' => implode(',',$request->skills),
                'status' => $request->status,
                'image' => $image_name
            );

            //dd($form_data);
            Student::find($request->id)->update($form_data);
            $notification = array(
                'message' => 'Student Successfully Updated!', 
                'alert-type' => 'success'
            );
            //return response()->json(['url'=>url('admin/students')]);
            //return response()->json($notification);
            return redirect('admin/students')->with($notification);
    }

    public function destroy($id)
    {
        $data = Student::findOrFail($id);
        $data->delete();
    }

    public function bulkremove(Request $request)
    {
        $student_id = $request->input('id');
        $student = Student::whereIn('id', explode(",", $student_id));
        $student->delete();
    }

    public function show_profile()
    {
        $student = Auth::guard('students')->check();
        return view('students.profile',compact('student'));
    }

    public function edit_profile($id)
    {
        $student = Student::find($id);   
        return view('students.edit_profile',compact('student'));
    }

    public function update_profile(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'gender' => 'required',
            'address' => 'required',
            'mobile' => 'required|numeric|digits:10',
            ]);

            $form_data = array(
                'id' => Auth::guard('students')->user()->id,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'gender' => $request->gender,
                'address' => $request->address,
                'mobile' => $request->mobile,
            );

            //dd($form_data);

            Student::find(Auth::guard('students')->user()->id)->update($form_data);
            $notification = array(
                'message' => 'Student Profile Updated Successfully !', 
                'alert-type' => 'success'
            );

            return redirect('student/profile')->with($notification);
    }

    public function show(Student $student)
    {
        $all_students = Student::all();
        return view('students.all_students',compact('all_students'));
    }

    public function show_user(Student $student)
    {
        $all_users = User::all();
        return view('students.all_users',compact('all_users'));
    }

    public function show_active(Student $student)
    {
        $active_students = Student::where('status', 'Active')->get();
        return view('students.active_students',compact('active_students'));
    }

    public function show_deactive(Student $student)
    {
        $deactive_students = Student::where('status', 'Deactive')->get();
        return view('students.deactive_students',compact('deactive_students'));
    }

    public function exportPDF() {
      $data = Student::all();
      view()->share('students',$data);
      $pdf = PDF::loadView('pdf.students_pdf_view', $data);

      //$pdf->save(storage_path('/documents/').'students_data_'.date('d-m-Y H:i:s').'.pdf');
      return $pdf->download('students_data.pdf');
    }

    public function exportPDF_users() {
      $data = User::all();
      view()->share('users',$data);
      $pdf = PDF::loadView('pdf.user_pdf_view', $data);

      //$pdf->save(storage_path('/documents/').'students_data_'.date('d-m-Y H:i:s').'.pdf');
      return $pdf->download('users_data.pdf');
    }

    public function exportExcel()
    {
      return Excel::download(new StudentExport, 'students.xlsx');
    }

    public function exportCSV()
    {
      return Excel::download(new StudentExport, 'students.csv');
    }

    public function import(Request $request) 
    {
        $path1 = $request->file('file')->store('temp'); 
        $path=storage_path('app').'/'.$path1;  
        $data = Excel::import(new StudentImport,$path);
        //Excel::import(new StudentImport, request()->file('file')->getRealPath());
        $notification = array(
                'message' => 'Import Successfully !', 
                'alert-type' => 'success'
            );
        return redirect('admin/students')->with($notification);
    }

    public function changeStatus(Request $request,$id,$status)
    {
        $student = Student::find($id);
        $updateStatus = array(
                'status' => $status,
                'updated_at' => date("Y-m-d H:i:s")
            );
        $student->where('id','=',$id)->update($updateStatus);
        $data = array(
            'msg'    => 'User status updated successfully.',
            'status' => 'success'
            );
        $request->session()->flash('succ_message',array('msg'=>$data['msg']));
        return $data;
    }
}

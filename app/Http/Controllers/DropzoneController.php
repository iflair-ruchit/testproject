<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\ImageUpload;
use DB;
use App\Student;
use Auth;

class DropzoneController extends Controller
{
    public function index()
    {
    	return view('students.gallery');
    }

   	public function upload(Request $request)
    {
      $user = Auth::guard('students')->user();
    	$image = $request->file('file');
	    $imageName = rand() . '.' . $image->extension();
	    $image->move(public_path('gallery/' . $user->id . '/'), $imageName);
	    $data  =   ImageUpload::create(['user_id' => $user->id,'image_name' => $imageName]);
	    
	    return response()->json(['success' => $data]);
    }
	
	 public function edit($id, $user_id)
    {
        $images = ImageUpload::find($user_id);
        return view('students.edit_gallery',compact('images'));        
    }

    public function update(Request $request)
    {
      $user = Auth::guard('students')->user();
	    $image = $request->file('file');
	    $imageName = rand() . '.' . $image->extension();
	    $image->move(public_path('gallery/' . $user->id . '/'), $imageName);

	    $form_data = array(
           'id' => $request->id,
           'user_id' => $user->id,
        );
	    ImageUpload::find($request->id)->update(['user_id' => $user->id,'image_name' => $imageName]);
	    
	    return response()->json(['success' => $form_data]);
    }

    public function delete($id)
    {
      $user = Auth::guard('students')->user();
	    $data = ImageUpload::findOrFail($id);
	    /*$image_path = public_path().'/gallery/'. $user->id . '/'.$data->image_name;
	    unlink($image_path);*/
      $data->delete();
    }

    public function fetch(Request $request)
    {
    $user = Auth::guard('students')->user();
     if($request->ajax())
     {
      if($request->id > 0)
      {
       $data = DB::table('image_uploads')
          ->where('id', '<', $request->id)
          ->where('user_id',$user->id)
          ->where('deleted_at', '=', NULL)
          ->orderBy('id', 'DESC')
          ->limit(6)
          ->get();
      }
      else
      {
       $data = DB::table('image_uploads')
       	  ->where('deleted_at', '=', NULL)
          ->where('user_id',$user->id)
          ->orderBy('id', 'DESC')
          ->limit(6)
          ->get();
      }
      $output = '';
      $last_id = '';
      
      if(!$data->isEmpty())
      {
        $output = '<div class="row">';
	    foreach($data as $row)
	    {
	      $output .= '
	      <div class="col-md-2 mt-2" style="margin-bottom:16px;" align="center">
	                <img src="'.asset('gallery/'. $row->user_id . '/' . $row->image_name).'" class="img-thumbnail" width="130" height="130" style="height:130px;" /><br>
	                <a href = "gallery/edit/'. $row->user_id. '/' .$row->id.'"><button type="button" name="edit" id="'
	                	.$row->id.'" 
                    class="edit btn btn-primary"><i class="fa fa-eye"></i>
                    </button></a>
	                <button type="button" class="btn btn-link remove_image" id="'.$row->id.'">
	                <i class="fa fa-close"></i></button>
	        </div>';
	        $last_id = $row->id;
	    }
       $output .= '
       <div id="load_more">
        <button type="button" name="load_more_button" class="btn btn-info" data-id="'.$last_id.'" id="load_more_button">Load More</button>
       </div>';
      }
      else
      {
       $output .= '
       <div id="load_more">
        <button type="button" name="load_more_button" class="btn btn-secondary">No Images Found</button>
       </div>';
      }
      echo $output;
     }
    }
}

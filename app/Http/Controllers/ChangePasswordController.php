<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\StdMatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\Student;
use Auth;
use Brian2694\Toastr\Facades\Toastr;

class ChangePasswordController extends Controller
{
	public function __construct()
	{
		$this->middleware('students')->except('logout');
	}

    public function index()
    {

      return view('students.change_password');
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'old_password' => ['required', new StdMatchOldPassword],
            'new_password' => 'required|min:6|different:old_password',
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        Student::find(Auth::guard('students')->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        $notification = array(
            'message' => 'Password successfully changed!', 
            'alert-type' => 'success'
        );

        return back()->with($notification);
    }
}
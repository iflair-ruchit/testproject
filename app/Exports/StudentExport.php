<?php

namespace App\Exports;

use App\Student;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class StudentExport implements FromCollection,WithHeadings,ShouldAutoSize,WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
	public function headings():array
	{
		return[
			"ID",
			"First Name",
			"Last Name",
			"Email",
			"Password",
			"Token",
			"Gender",
			"Address",
			"Mobile",
			"Skills",
			"Status"
		];
	}

    public function collection()
    {
        return Student::all();
     	//return collect(Student::getStudents());
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
		    },
		];
    }
}

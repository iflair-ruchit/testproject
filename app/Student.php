<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Student extends Authenticatable
{
	use Notifiable;

    use SoftDeletes;

    use Sluggable;

	protected $guard = 'students';
    
    protected $table = 'students';
    
    protected $fillable = ['first_name','last_name','slug','email','password','token','gender','address','latitude','longitude','mobile','skills','status','image',
    ];

    public static function getStudents()
    {
    	$records = DB::table('students')->select("id","first_name","last_name","email","gender","mobile","address", "status")->orderBy('id','asc')->get()->toArray();
    	return $records;
    }

    public function sluggable(){
        return[
            'slug' => [
                'source' => 'first_name'
            ]
        ];
    }
}
